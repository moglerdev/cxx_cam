
#ifndef __HELPER_HPP__
#define __HELPER_HPP__

#include <vector>
#include <string>
#include <opencv2/core.hpp>

namespace CxxCam
{
    int split(std::vector<std::string>& res , std::string s, std::string delimiter);
    
    bool string_to_size(std::string &in, cv::Size& size);

} // namespace CxxCam

#endif // __HELPER_HPP__
