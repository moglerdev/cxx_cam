
#ifndef __SERVER_HPP__
#define __SERVER_HPP__

#include <crow.h>
#include <mutex>

#include "camera.hpp"
#include "connection.hpp"

namespace CxxCam {

    class Server {
    private:
        mutable std::mutex mtx;
        crow::SimpleApp app;
        std::map<void*, bool> authed;
        std::thread idle_thread {};

        Camera cam {};

        bool camera_on = false;

        std::string blob;

    public:
        Server() = default;
        ~Server() = default;

        bool idle_loop();
        bool stop_camera();
        bool start_camera();

        void run(int port = 8080);
    };
    
}

#endif // __SERVER_HPP__
