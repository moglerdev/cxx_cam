
#include "server.hpp"
#include "helper.hpp"

#include <thread>

namespace CxxCam {


    bool Server::idle_loop() {
        std::lock_guard<std::mutex> guard(this->mtx);
        bool error = true;
        try {
            while(this->camera_on) {
                this->cam.read_frame(this->blob, cv::Size(640, 480));
                // timer sleep
                std::this_thread::sleep_for(std::chrono::milliseconds(33));
            }
            error = false;
        }
        catch (std::exception& e) {
            std::cerr << "[ERROR] " << e.what() << std::endl;
        }
        this->camera_on = false;
        return error;
    }

    bool Server::stop_camera() {
        camera_on = false;
        idle_thread.join();
        this->cam.stop();
        return true;
    }

    bool Server::start_camera() {
        if (!this->mtx.try_lock() || this->camera_on) {
            std::cerr << "[ERROR] Could not start camera\n";
            return false;
        }
        this->cam.start();
        camera_on = true;
        idle_thread = std::thread(&Server::idle_loop, this);
        return true;
    }

    void Server::run(int port) {
        this->blob = std::string(640 * 480 * 3, 0); // 640x480x3

        //define yo ur endpoint at the root directory
        CROW_ROUTE(app, "/")([](){
            auto page = crow::mustache::load_text("index.html");
            return page;
        });

        // std::unordered_set<crow::websocket::connection&> wsConns;

        CROW_ROUTE(app, "/ws").websocket()
            .onopen([&](crow::websocket::connection& conn) {
                conn.userdata();
            })
            .onclose([&](crow::websocket::connection& conn, const std::string& reason) {
                // wsConns.erase(std::remove(wsConns.begin(), wsConns.end(), conn), wsConns.end());
                this->authed.erase(conn.userdata());
            })
            .onmessage([&](crow::websocket::connection& conn, const std::string& data, bool is_binary) {
                if (this->authed.find(conn.userdata()) == this->authed.end()) {
                    if (data.rfind("auth:", 0) == 0) {
                        if (data.substr(5) == "12345") { // TODO: dynamic password
                            conn.send_text("authed");
                            this->authed[conn.userdata()] = true;
                        } else {
                            conn.send_text("unauthed");
                        }
                    } else {
                        conn.send_text("unauthed");
                    }
                    return;
                } 
                if (data == "start") {
                    if (start_camera()) {
                        conn.send_text("started");
                    } else {
                        conn.send_text("failed");
                    }
                } else if (data == "stop") {
                    if (stop_camera()) {
                        conn.send_text("stopped");
                    } else {
                        conn.send_text("failed");
                    }
                } else if (data == "blob") {
                    conn.send_binary(blob);
                } else {
                    conn.send_text("unknown");
                }
            });


        app.port(port).multithreaded().run();
    }
}
