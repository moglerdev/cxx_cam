
#ifndef __CONNECTION_HPP__
#define __CONNECTION_HPP__

#include <crow.h>

namespace CxxCam {
    class Connection
    {
    private:
        crow::websocket::connection& conn;
        // user data
        // is authenticated
        // is admin
    public:
        Connection(crow::websocket::connection& conn) : conn(conn) {}
        ~Connection() = default;

        bool operator==(crow::websocket::connection& conn) const {
            return conn.userdata() == this->conn.userdata();
        }
    };
}


#endif // __CONNECTION_HPP__
