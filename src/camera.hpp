
#ifndef __CAMERA_HPP__
#define __CAMERA_HPP__

#include <opencv2/videoio.hpp>
#include <crow.h>

namespace CxxCam {

    class Camera {

    private:
        cv::VideoCapture cam {};
        mutable std::mutex mtx {};
        int deviceId;

    public: 
        Camera(int _deviceId = -1) : deviceId(_deviceId) {};
        ~Camera() = default;

        bool start();
        bool stop();
        bool read_frame(cv::Mat& frame, cv::Size size);
        bool read_frame(std::string &out, cv::Size size);
    };

}

#endif // __CAMERA_HPP__
