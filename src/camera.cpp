
#include "camera.hpp"
#include <iostream>

#include <opencv2/core.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

namespace CxxCam {

    bool Camera::start() {
        int apiId = cv::CAP_ANY;

        std::lock_guard<std::mutex> guard(this->mtx);

        if (!this->cam.open(this->deviceId, apiId) || !cam.isOpened()) {
            std::cerr << "ERROR! Unable to open camera\n";
            return false;
        }

        return true;
    }

    bool Camera::read_frame(cv::Mat& out, cv::Size size) {
        std::lock_guard<std::mutex> guard(this->mtx);

        cv::Mat org;

        // wait for a new out from camera and store it into 'out'
        if (!this->cam.read(org)) {
            std::cerr << "ERROR! couldn't read from Video Capture!\n";
            return false;
        }

        cv::resize(org, out, size, 0, 0, cv::INTER_AREA);
        org.release();

        return true;
    }

    bool Camera::read_frame(std::string& out, cv::Size size)
    {
        cv::Mat org;
        std::vector<unsigned char>jpeg;
        if (!this->read_frame(org, size) || !cv::imencode(".jpg", org, jpeg)) {
            std::cerr << "[ERROR] Could not convert to jpeg\n";
            return false;
        }
        org.release();

        std::string blob(jpeg.begin(), jpeg.end());
        out = blob;
        return true;
    }

    bool Camera::stop() {
        // std::lock_guard<std::mutex> guard(this->mtx);

        // this->cam.
        return true;
    }
}
