
#include "helper.hpp"
#include <iostream>
 

namespace CxxCam
{
    int split(std::vector<std::string>& res , std::string s, std::string delimiter) {
        size_t pos = 0;
        std::string token;
        if(s == "") return 0;
        int counter = 1;
        while ((pos = s.find(delimiter)) != std::string::npos) {
            token = s.substr(0, pos);
            res.push_back(token);
            s.erase(0, pos + delimiter.length());
            ++counter;
        }
        res.push_back(s);
        return counter;
    }

    bool string_to_size(std::string &in, cv::Size& size) {
        std::vector<std::string> wh;
        if (split(wh, in, ":") < 2) {
            std::cerr << "[ERROR] Wrong width and height\n";
            return false;
        }

        if (wh.size() < 2) {
            return false;
        }
        
        int w = std::stoi(wh.at(0));
        int h = std::stoi(wh.at(1));
        size = cv::Size { w, h };
        return true;
    }
} // namespace CxxCam
