FROM alpine:3.18.0 AS build

RUN apk update && \
    apk add --no-cache \
        build-base \
        cmake \
        boost boost-dev \
        wget \
        tar \
        opencv opencv-dev

WORKDIR /fetch

RUN wget https://github.com/CrowCpp/Crow/releases/download/v1.0%2B5/crow-v1.0+5.tar.gz -O crow.tar.gz
RUN tar -xvf ./crow.tar.gz
RUN cp -ar ./include/* /usr/include
RUN rm ./include -rf
RUN cp -ar ./lib/* /usr/lib
RUN rm ./lib -rf

WORKDIR /app

COPY src/ ./src/
COPY CMakeLists.txt .
    

WORKDIR /app/build
ENV LD_LIBRARY_PATH=/usr/local/lib
RUN cmake ..
RUN cmake --build . --config Debug --target all -j 14 --


FROM alpine:3.18.0

RUN apk update && \
    apk add --no-cache \
        boost \
        opencv \
        libstdc++

RUN addgroup -S cxx && adduser -S cxx -G cxx
USER cxx

COPY --chown=cxx:cxx --from=build \
    ./app/build/cxx_cam \
    ./app/

EXPOSE 18080
ENTRYPOINT [ "./app/cxx_cam" ]
